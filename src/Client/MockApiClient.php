<?php

declare(strict_types=1);

namespace App\Client;

use App\Contract\Client\MockApiClientInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MockApiClient implements MockApiClientInterface
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function get(string $subdomain, string $endpoint): array
    {
        $url = $this->buildUrl($subdomain, $endpoint);
        $response = $this->client->request('GET', $url);

        return $response->toArray();
    }

    private function buildUrl(string $subdomain, string $endpoint): string
    {
        return
            \sprintf(
                'https://%s.mockapi.io/%s',
                $subdomain,
                $endpoint
            )
        ;
    }
}
