<?php

declare(strict_types=1);

namespace App\Command\PromoCode;

use App\Contract\Repository\OfferRepositoryInterface;
use App\Contract\Repository\PromoCodeRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class ValidateCommand extends Command
{
    public static $defaultName = 'promo-code:validate';
    private PromoCodeRepositoryInterface $promoCodeRepository;
    private OfferRepositoryInterface $offerRepository;

    public function __construct(PromoCodeRepositoryInterface $promoCodeRepository, OfferRepositoryInterface $offerRepository)
    {
        $this->promoCodeRepository = $promoCodeRepository;
        $this->offerRepository = $offerRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Check promo code validity');

        $this->addArgument('code', InputArgument::REQUIRED, 'The code value');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $code = $this->promoCodeRepository->getByCode($input->getArgument('code'));

        $dateTime = new \DateTime($code['endDate']);
        $current = new \DateTime();

        if ($dateTime <= $current) {
            $io->error('This promo code is outdated.');
            return 0;
        }

        $offers = $this->offerRepository->getByPromoCode($code['code']);

        if (empty($offers) === true)
        {
            $io->error('This promo code has no offers.');
            return 0;
        }

        $content = $this->format($code, $offers);

        $this->writeFile($code['code'], $content);
        $io->success('File has been write.');

        return 0;
    }

    private function format(array $code, array $offers): array
    {
        $offersFormatted = [];

        foreach ($offers as $offer) {
            $offersFormatted[] = [
                'name' => $offer['offerName'],
                'type' => $offer['offerType'],
            ];
        }

        return [
            'promoCode' => $code['code'],
            'endDate' => $code['endDate'],
            'discountValue' => $code['discountValue'],
            'compatibleOfferList' => $offersFormatted,
        ];
    }

    protected function writeFile($code1, array $content): void
    {
        $fileSystem = new Filesystem();
        $filename = sprintf(
            '%s.json',
            $code1
        );

        $fileSystem->remove($filename);
        $fileSystem->appendToFile($filename, \json_encode($content));
    }
}
