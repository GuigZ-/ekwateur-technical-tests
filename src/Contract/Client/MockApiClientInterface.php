<?php

declare(strict_types=1);

namespace App\Contract\Client;

interface MockApiClientInterface
{
    public function get(string $subdomain, string $endpoint): array;
}
