<?php

declare(strict_types=1);

namespace App\Contract\Repository;

interface OfferRepositoryInterface
{
    public function getAll(): array;

    public function getByPromoCode(string $promoCode): array;
}
