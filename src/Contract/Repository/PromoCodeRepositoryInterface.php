<?php

declare(strict_types=1);

namespace App\Contract\Repository;

interface PromoCodeRepositoryInterface
{
    public function getAll(): array;

    public function getByCode(string $code): array;
}
