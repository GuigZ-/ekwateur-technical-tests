<?php

declare(strict_types=1);

namespace App\Ekwateur\Client;

use App\Contract\Client\MockApiClientInterface;

class MockApiClient
{
    private string $mockApiClientId;
    private MockApiClientInterface $client;

    public function __construct(string $mockApiClientId, MockApiClientInterface $client)
    {
        $this->mockApiClientId = $mockApiClientId;
        $this->client = $client;
    }

    public function get(string $endpoint): array
    {
        return $this->client->get($this->mockApiClientId, $endpoint);
    }
}
