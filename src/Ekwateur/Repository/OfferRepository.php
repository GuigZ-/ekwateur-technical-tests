<?php

declare(strict_types=1);

namespace App\Ekwateur\Repository;

use App\Contract\Repository\OfferRepositoryInterface;
use App\Ekwateur\Client\MockApiClient;

class OfferRepository implements OfferRepositoryInterface
{
    private MockApiClient $client;

    public function __construct(MockApiClient $client)
    {
        $this->client = $client;
    }

    public function getAll(): array
    {
        return $this->client->get('offerList');
    }

    public function getByPromoCode(string $promoCode): array
    {
        $offers = $this->getAll();

        $response = [];

        foreach ($offers as $offer) {
            if ($this->offerHasPromoCode($offer, $promoCode) === false) {
                continue;
            }

            $response[] = $offer;
        }

        return $response;
    }

    private function offerHasPromoCode(array $offer, string $promoCode): bool
    {
        if (empty($offer['validPromoCodeList']) === true) {
            return false;
        }

        return \in_array($promoCode, $offer['validPromoCodeList'], true) === true;
    }
}
