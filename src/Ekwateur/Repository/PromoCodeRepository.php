<?php

declare(strict_types=1);

namespace App\Ekwateur\Repository;

use App\Contract\Repository\PromoCodeRepositoryInterface;
use App\Ekwateur\Client\MockApiClient;
use App\Exception\NotFoundException;

class PromoCodeRepository implements PromoCodeRepositoryInterface
{
    private MockApiClient $client;

    public function __construct(MockApiClient $client)
    {
        $this->client = $client;
    }

    public function getAll(): array
    {
        return $this->client->get('promoCodeList');
    }

    public function getByCode(string $code): array
    {
        $codes = $this->getAll();

        foreach ($codes as $c) {
            if ($c['code'] === $code) {
                return $c;
            }
        }

        throw new NotFoundException();
    }
}
