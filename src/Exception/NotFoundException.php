<?php

declare(strict_types=1);

namespace App\Exception;

use Throwable;

class NotFoundException extends \Exception
{
    public function __construct($message = null, $code = 0, Throwable $previous = null)
    {
        $message = $message ?? 'Element not found';

        parent::__construct($message, $code, $previous);
    }
}
