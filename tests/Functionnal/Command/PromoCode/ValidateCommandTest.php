<?php

declare(strict_types=1);

namespace App\Tests\Functionnal\Command\PromoCode;

use App\Command\PromoCode\ValidateCommand;
use App\Exception\NotFoundException;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ValidateCommandTest extends KernelTestCase
{
    public function testExecuteSuccess(): void
    {
        $output = $this->apply('ALL_2000');

        static::assertStringContainsString('[OK] File has been write.', $output);
    }

    public function testExecuteThrowElementNotFound(): void
    {
        $this->expectException(NotFoundException::class);
        $this->expectExceptionMessage('Element not found');

        $this->apply('lorem-ipsum');
    }

    public function testExecuteThrowOutdated(): void
    {
        $output = $this->apply('EKWA_WELCOME');

        static::assertStringContainsString('[ERROR] This promo code is outdated.', $output);
    }

    private function apply(string $code): string
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find(ValidateCommand::$defaultName);
        $commandTester = new CommandTester($command);

        $commandTester->execute([
            'code' => $code
        ]);

        return $commandTester->getDisplay();
    }
}
