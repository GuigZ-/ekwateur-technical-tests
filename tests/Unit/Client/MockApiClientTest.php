<?php

declare(strict_types=1);

namespace App\Tests\Unit\Client;

use App\Client\MockApiClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MockApiClientTest extends TestCase
{
    /** @var MockObject|HttpClientInterface */
    private MockObject $client;

    private MockApiClient $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(HttpClientInterface::class);
        $this->service = new MockApiClient($this->client);
    }

    public function testGet(): void
    {
        $response = $this->createMock(ResponseInterface::class);

        $this->client
            ->expects(static::once())
            ->method('request')
            ->with('GET', 'https://lorem.mockapi.io/ipsum')
            ->willReturn($response);

        $response
            ->expects(static::once())
            ->method('toArray')
            ->willReturn(['lorem' => 'ipsum']);

        $actual = $this->service->get('lorem', 'ipsum');

        static::assertEquals(['lorem' => 'ipsum'], $actual);
    }
}
