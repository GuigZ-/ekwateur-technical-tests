<?php

declare(strict_types=1);

namespace App\Tests\Unit\Ekwateur\Client;

use App\Client\MockApiClient;
use App\Ekwateur\Client\MockApiClient as EkwateurClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MockApiClientTest extends TestCase
{
    private string $clientId;
    /** @var MockApiClient|MockObject */
    private $client;

    private EkwateurClient $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->clientId = 'lorem-ipsum';
        $this->client = $this->createMock(MockApiClient::class);
        $this->service = new EkwateurClient($this->clientId, $this->client);
    }

    public function testGet(): void
    {
        $this->client->expects(static::once())
            ->method('get')
            ->with($this->clientId, 'lorem-ipsum')
            ->willReturn(['lorem' => 'ipsum']);

        $actual = $this->service->get('lorem-ipsum');

        static::assertEquals(['lorem' => 'ipsum'], $actual);
    }
}
