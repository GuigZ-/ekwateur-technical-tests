<?php

declare(strict_types=1);

namespace App\Tests\Unit\Ekwateur\Repository;

use App\Ekwateur\Client\MockApiClient;
use App\Ekwateur\Repository\OfferRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class OfferRepositoryTest extends TestCase
{
    /** @var MockApiClient|MockObject */
    private $client;
    private OfferRepository $service;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(MockApiClient::class);
        $this->service = new OfferRepository($this->client);
    }

    public function testGetAll(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('offerList')
                     ->willReturn(['lorem' => 'ipsum']);

        $actual = $this->service->getAll();

        static::assertEquals(['lorem' => 'ipsum'], $actual);
    }

    public function testGetByPromoCodeNoOffers(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('offerList')
                     ->willReturn([]);

        $actual = $this->service->getByPromoCode('lorem');

        static::assertEquals([], $actual);
    }

    public function testGetByPromoCodeNoValidPromoCodeList(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('offerList')
                     ->willReturn(
                         [
                             ['validPromoCodeList' => ['ipsum']],
                         ]
                     );

        $actual = $this->service->getByPromoCode('lorem');

        static::assertEquals([], $actual);
    }

    public function testGetByPromoCodeSuccess(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('offerList')
                     ->willReturn(
                         [
                             ['validPromoCodeList' => ['lorem']],
                         ]
                     );

        $actual = $this->service->getByPromoCode('lorem');

        static::assertEquals(
            [
                ['validPromoCodeList' => ['lorem']],
            ],
            $actual
        );
    }
}
