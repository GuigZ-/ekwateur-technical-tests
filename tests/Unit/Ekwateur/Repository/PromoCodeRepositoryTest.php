<?php

declare(strict_types=1);

namespace App\Tests\Unit\Ekwateur\Repository;

use App\Ekwateur\Client\MockApiClient;
use App\Ekwateur\Repository\PromoCodeRepository;
use App\Exception\NotFoundException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PromoCodeRepositoryTest extends TestCase
{
    /** @var MockApiClient|MockObject */
    private $client;
    private PromoCodeRepository $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = $this->createMock(MockApiClient::class);
        $this->service = new PromoCodeRepository($this->client);
    }

    public function testGetAll(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('promoCodeList')
                     ->willReturn(['lorem' => 'ipsum']);

        $actual = $this->service->getAll();

        static::assertEquals(['lorem' => 'ipsum'], $actual);
    }

    public function testGetByCodeNotFoundWithEmptyList(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('promoCodeList')
                     ->willReturn([]);

        $this->expectException(NotFoundException::class);
        $this->expectDeprecationMessage('Element not found');

        $this->service->getByCode('lorem');
    }

    public function testGetByCodeNotFound(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('promoCodeList')
                     ->willReturn([['code' => 'ipsum']]);

        $this->expectException(NotFoundException::class);
        $this->expectDeprecationMessage('Element not found');

        $this->service->getByCode('lorem');
    }

    public function testGetByCode(): void
    {
        $this->client->expects(static::once())
                     ->method('get')
                     ->with('promoCodeList')
                     ->willReturn([['code' => 'lorem']]);

        $actual = $this->service->getByCode('lorem');
        static::assertEquals(['code' => 'lorem'], $actual);
    }
}
